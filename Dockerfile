FROM node:16-alpine
WORKDIR sprint4
COPY . .
RUN npm install
CMD [ "npm", "run","start"]

