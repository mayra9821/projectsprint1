const Joi = require( 'joi');

const loginSchema = Joi.object({
    email: Joi.string()
        .email({
            minDomainSegments: 2,
            tlds: {allow: ['com', 'net', 'co']},
        }),
    pass: Joi.string()
        .pattern(new RegExp(RegExp('^[a-zA-Z0-9]{3,30}$'))), 
});

module.exports = loginSchema;