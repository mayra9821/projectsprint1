const Joi = require( 'joi');

const usuarioSchema = Joi.object({
    nombreUsuario: Joi.string()
        .alphanum()
        .required(),

    pass: Joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    
    repetir_pass: Joi.ref('pass'),

    access_token: [
        Joi.string(),
        Joi.number()
    ],
    direccion: Joi.string(),
    telefono: Joi.string(),
    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }) ,
    
    isAdmin: Joi.boolean(),
})
module.exports= usuarioSchema;