const db = require( '../utils/db');
const bcrypt = require( 'bcrypt');
const config = require( '../config/config');

const usuarioDefault = async () => {
  await db.Usuario.findOrCreate({
    where:
    {
      email: config.module.ADMINEMAIL,
      isAdmin: true

    },
    defaults: 
            {
            nombreUsuario: 'Admin',
            email: config.module.ADMINEMAIL,
            pass: bcrypt.hashSync(config.module.ADMINPASS, 10),
            telefono: '000000000',
            isAdmin: true
            }
        });

  

};


module.exports = usuarioDefault;
