const db = require( '../utils/db');

const productoDefault = async () => {
  await db.Producto.findOrCreate({
    where:
    {
      nombre : "PERRO"
    },
    defaults:
    {
      nombre : "PERRO",
      precio : 5000
    }
  });
  await db.Producto.findOrCreate({
    where:
    {
      nombre : "SALCHIPAPA"
    },
    defaults:
    {
      nombre : "SALCHIPAPA",
      precio : 10000
    }
  });
  await db.Producto.findOrCreate({
    where:
    {
      nombre : "PIZZA"
    },
    defaults:
    {
      nombre : "PIZZA",
      precio : 15000
    }
  });
  await db.Producto.findOrCreate({
    where:
    {
      nombre : "MAZORCA"
    },
    defaults:
    {
      nombre : "MAZORCA",
      precio : 15000
    }
  });
  await db.Producto.findOrCreate({
    where:
    {
      nombre : "HAMBURGUESA"
    },
    defaults:
    {
      nombre : "HAMBURGUESA",
      precio : 20000
    }
  });

};


module.exports = productoDefault;
