const db = require( '../utils/db');

const estadoDefault = async () => {
  await db.Estado.findOrCreate({
    where:
    {
      nombre: 'NUEVO',
    },
    defaults:
    {
      nombre: 'NUEVO',
    }
  });
  await db.Estado.findOrCreate({
    where:
    {
      nombre: 'ACEPTADO',
    },
    defaults:
    {
      nombre: 'ACEPTADO',
    }
  });
  await db.Estado.findOrCreate({
    where:
    {
      nombre: 'EN PREPARACION',
    },
    defaults:
    {
      nombre: 'EN PREPARACION',
    }
  });
  await db.Estado.findOrCreate({
    where:
    {
      nombre: 'ENVIADO',
    },
    defaults:
    {
      nombre: 'ENVIADO',
    }
  });
  await db.Estado.findOrCreate({
    where:
    {
      nombre: 'ENTREGADO',
    },
    defaults:
    {
      nombre: 'ENTREGADO',
    }
  });
  await db.Estado.findOrCreate({
    where:
    {
      nombre: 'CANCELADO',
    },
    defaults:
    {
      nombre: 'CANCELADO',
    }
  });

};


module.exports = estadoDefault;
