const db = require( '../utils/db');

const metodoPagoDefault = async () => {
  await db.MetodoPago.findOrCreate({
    where:
    {
      metodo: 'Tarjeta Credito',
    },
    defaults:
    {
      metodo: 'Tarjeta Credito',
    }
  });
  await db.MetodoPago.findOrCreate({
    where:
    {
      metodo: 'Tarjeta debito',
    },
    defaults:
    {
      metodo: 'Tarjeta debito',
    }
  });
  await db.MetodoPago.findOrCreate({
    where:
    {
      metodo: 'Efectivo',
    },
    defaults:
    {
      metodo: 'Efectivo',
    }
  });
  await db.MetodoPago.findOrCreate({
    where:
    {
      metodo: 'Nequi',
    },
    defaults:
    {
      metodo: 'Nequi',
    }
  });
  await db.MetodoPago.findOrCreate({
    where:
    {
      metodo: 'Transferencia',
    },
    defaults:
    {
      metodo: 'Transferencia',
    }
  });

};


module.exports = metodoPagoDefault;
