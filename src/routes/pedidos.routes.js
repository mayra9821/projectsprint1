const express = require( 'express');
const verifyAdmin = require( '../middlewares/auth');
const db = require( '../utils/db');

const pedidosRouter = express.Router();

/**
 * @swagger
 * /pedidos/admin:
 *  get:
 *      summary: Obtener la lista de pedidos con credenciales de admin
 *      tags: [Administradores]
 *      responses:
 *          '200':
 *              description: JSON con lista de pedidos                       
 */
pedidosRouter.get('/admin', verifyAdmin, async (req, res) => {
    const getPedidos = await db.Pedido.findAll();
    res.json(getPedidos);
});

/**
 * @swagger
 * /pedidos:
 *  get:
 *      summary: Obtener la lista de pedidos sin credenciales
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: JSON con lista de pedidos                       
 */
pedidosRouter.get('/', async (req, res) => {
    const getPedidos = await db.Pedido.findAll({ where: { id: req.user.id }, include: [
        { model: db.Producto },
        {
            model: db.Direccion, as: "Direccion"
        }] });
    res.json(getPedidos);
});

/**
 * @swagger
 * /pedidos/{id}:
 *  get:
 *      summary: Obtener la lista de pedidos por id
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: JSON con lista de pedidos                       
 */
pedidosRouter.get('/:id', async (req, res) => {
    const getPedidos = await db.Pedido.findByPk(req.params.id, {
        include: [
            { model: db.Producto }
            ,
            {
                model: db.Direccion, as: "Direccion"
            }]
    });
    if (getPedidos) {
        res.json(getPedidos)
    } else {
        res.json("No existe usuario con ese id")
    }
});

/**
 * @swagger
 * /pedidos/usuario/{id}:
 *  get:
 *      summary: Obtener la lista de pedidos del usuario por id
 *      tags: [Pedidos]
 *      responses:
 *          '200':
 *              description: JSON con lista de pedidos                       
 */
pedidosRouter.get('/usuario/:id', async (req, res) => {
    const getPedidos = await db.Pedido.findByPk(req.params.id, {
        include: [
            { model: db.Producto },
            {
                model: db.Direccion, as: "Direccion" 
            }

        ]
    });
    if (getPedidos) {
        res.json(getPedidos)
    } else {
        res.json("No existe usuario con ese id")
    }
});


/**
 * @swagger
 * paths:
 *  /pedidos/nuevo:
 *      post:
 *          summary: Añadir nuevo pedido
 *          tags: [Pedidos]
 *          requestBody: 
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              direccion: 
 *                                  type: string
 *                              metodopagoId:
 *                                  type: integer
 *                              estadoId:
 *                                  type: integer
 *                              productosxpedido:
 *                                  type: array
 *                                  items:
 *                                      type: object
 *                                      properties:
 *                                          productoId: 
 *                                              type:integer
 *                                          cantidad: 
 *                                              type:integer
 *                              
 *                          example: 
 *                             direccion: probando
 *                             metodopagoId: 1
 *                             estadoId: 1
 *                             productosxpedido: 
 *                              [
 *                               {
 *                                   "productoId": 1,
 *                                   "cantidad": 2     
 *                               },
 *                               {
 *                                   "productoId":2, 
 *                                   "cantidad": 4
 *                               }
 *                              ]
 *          
 *          responses:
 *              '201':
 *                  description: Usuario suspendido con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */


pedidosRouter.post('/nuevo', async (req, res) => {
    // console.log(req.user.id)
    const user = await db.Usuario.findOne({
        where: req.user.id,
        include: [
            {
                model: db.Direccion, as: "Direccion"
            }
        ]
    });
    // console.log(user)
    const pedidoNuevo = await db.Pedido.create({
        usuarioId: user.id,
        estadoId: 1,
        metodopagoId: req.body.metodopagoId
    })
    // console.log({ usuarioId: user.id, direccion: req.body.direccion })
    if (pedidoNuevo) {
        if (req.body.direccion) {
            const direccion = await db.Direccion.findOrCreate({
                where: { usuarioId: user.id, direccion: req.body.direccion },
                defaults: {
                    usuarioId: user.id, direccion: req.body.direccion
                }
            });
            pedidoNuevo.direccionId = direccion[0].id

        } else {
            pedidoNuevo.direccionId = user.Direccion[0].id
        }
        // console.log(user)
        // console.log(pedidoNuevo.direccionId)
        let total = 0
        for (const x of req.body.productosxpedido) {
            await pedidoNuevo.addProducto(x.productoId, { through: { cantidad: x.cantidad } })
            const producto = await db.Producto.findByPk(x.productoId)
            total += (producto.precio * x.cantidad)
        }

        pedidoNuevo.total = total
        // console.log(pedidoNuevo.total)
        await pedidoNuevo.save()


        res.json(await db.Pedido.findByPk(pedidoNuevo.id, {
            include: [
                { model: db.Producto },
                { model: db.Estado },
                { model: db.MetodoPago },
                { model: db.Direccion, as:"Direccion" },]
        }
        ))
    } else {
        res.json("No Se ha creado el pedido")
    }
});

/**
 * @swagger
 * paths:
 *  /pedidos/modificar/{id}:
 *      put:
 *          summary: modificar pedido
 *          requestBody: 
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              direccion: 
 *                                  type: string
 *                              metodopagoId:
 *                                  type: integer
 *                              estadoId:
 *                                  type: integer
 *                              productosxpedido:
 *                                  type: array
 *                                  items:
 *                                      type: object
 *                                      properties:
 *                                          productoId: 
 *                                              type:integer
 *                                          cantidad: 
 *                                              type:integer
 * 
 *                                      
 *                              
 *                          example: 
 *                             direccion: probando
 *                             metodopagoId: 1
 *                             estadoId: 1
 *                             productosxpedido: 
 *                                [
 *                                  {
 *                                      "productoId": 2,
 *                                      "cantidad": 4     
 *                                  }
 *                                ]
 *          tags: [Pedidos]
 *          responses:
 *              '201':
 *                  description: pedido modificado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */
pedidosRouter.put('/modificar/:id', async (req, res) => {

    // const user = await db.User.findByPk(req.user.id);
    const pedido = await db.Pedido.findByPk(req.params.id);
    const estado = await pedido.getEstado()
    if (estado.id !== 1) res.status(404).json("Pedido ya Aceptado , NO SE PUEDE MODIFICAR")
    pedido.direccion = req.body.direccion ? req.body.direccion : pedido.direccion
    // console.log(await pedido.getMetodopago())
    if (req.body.metodopagoId) await pedido.setMetodopago(req.body.metodopagoId)
    await pedido.setProductos([])
    let total = 0
    for (const x of req.body.productosxpedido) {
        await pedido.addProducto(x.productoId, { through: { cantidad: x.cantidad } })
        const producto = await db.Producto.findByPk(x.productoId)
        total += (producto.precio * x.cantidad)
    }

    pedido.total = total
    await pedido.save()
    res.json(await db.Pedido.findByPk(pedido.id, {
        include: [
            { model: db.Producto},
            { model: db.Direccion, as: "Direccion"}
        ]
    }))
})

/**
 * @swagger
 * paths:
 *  /pedidos/finalizar/{id}:
 *      put:
 *          summary: modificar pedido como enviado
 *          tags: [Pedidos]
 *          responses:
 *              '201':
 *                  description: pedido modificado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */

pedidosRouter.put('/finalizar/:id', async (req, res) => {
    const pedido = await db.Pedido.findByPk(req.params.id);
    pedido.setEstado(2)
    await pedido.save()
    res.json(await db.Pedido.findByPk(pedido.id, {
        include: [
            { model: db.Producto },
            { model: db.Direccion, as: "Direccion"}

        ]
    }))
})

/**
 * @swagger
 * paths:
 *  /pedidos/cambiarEstado/{id}:
 *      put:
 *          summary: modificar estado pedido
 *          tags: [Administradores]
 *          responses:
 *              '201':
 *                  description: pedido modificado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */

pedidosRouter.put('/cambiarEstado/:id', verifyAdmin, async (req, res) => {
    const pedido = await db.Pedido.findByPk(req.params.id);
    pedido.setEstado(req.body.estado)
    await pedido.save()
    res.json(await db.Pedido.findByPk(pedido.id, {
        include: [
            { model: db.Producto },
            { model: db.Direccion, as: "Direccion"}
        
        ]
    }))
})

module.exports = pedidosRouter;
