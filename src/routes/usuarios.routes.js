const express = require( 'express');
const jwt = require( 'jsonwebtoken');
const usuarioSchema = require( '../schemas/Usuario.joi');
const loginSchema = require( '../schemas/login.joi');
const db = require( '../utils/db');
const bcrypt = require( 'bcrypt');
const config = require( '../config/config');
const verifyAdmin = require( '../middlewares/auth');

const usuarioRouter = express.Router();

usuarioRouter.use(express.json());

/**
 * @swagger
 * paths:
 *  /usuarios:
 *      get:
 *          summary: devuelve usuarios, debe estar autenticado como administrador
 *          tags: [Administradores]
 *          responses:
 *          '200':
 *              description: JSON con lista de productos disponibles
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *          
 *      
 */
usuarioRouter.get('/', verifyAdmin, async (req, res) => {
    const usuarios = await db.Usuario.findAll();
    res.json(usuarios);
    // console.log(usuarios);
});


/**
 * @swagger
 * paths:
 *  /usuarios/registro:
 *      post:
 *          summary: Crea nuevo usuario
 *          security: []
 *          tags: [Usuarios] 
 *          requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              nombreUsuario:
 *                                  type: string
 *                              email: 
 *                                  type: string
 *                              pass:
 *                                  type: string
 *                              direccion:
 *                                  type: string
 *                              telefono:
 *                                  type: string
 *                          example:
 *                              nombreUsuario: 'mayra123'
 *                              email: mayra123@nada.com
 *                              pass: 'mayra123'
 *                              direccion: 'av 23 iiddv'
 *                              telefono: '32056236521'
 *          
 *          responses:
 *              '201':
 *                  description: Usuario creado
 *                  content:
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 *              '400': 
 *                  description: Usuario mal ingresado
 *                  content: 
 *                      application/json:
 *                          schema:
 *                              type: object
 *                              properties: 
 *                                  err:
 *                                      type: string     
 *                                        
 */

usuarioRouter.post('/registro', async (req, res) => {
    try {
        // console.log(req.body);
        const { nombreUsuario, pass, email, isAdmin } =
            await usuarioSchema.validateAsync(req.body);

        const usuarioCreado = await db.Usuario.create({
            nombreUsuario,
            email,
            pass: bcrypt.hashSync(pass, 10),
            isAdmin
        });
        if (req.body.telefono){
            usuarioCreado.telefono= req.body.telefono;
            usuarioCreado.save();
        }
        usuarioCreado.createDireccion({ direccion: req.body.direccion })
        console.log(usuarioCreado);
        // await db.Direccion.create({
        //     usuarioCreado, req.body.direccion})
        res.json(usuarioCreado);
    } catch (error) {
        console.log(error);
        res.status(400).json(error.details[0].message);
    }
});
/**
 * @swagger
 * paths:
 *  /usuarios/agenda/{id}:
 *      post:
 *          summary: Agenda de direcciones para cada usuario
 *          requestBody: 
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email: 
 *                                  type: string
 *                              pass: 
 *                                  type: string
 *                          example: 
 *                              email: mayra123@nada.com
 *                              pass: 'mayra123'
 *          tags: [Usuarios]
 *          responses:
 *              '201':
 *                  description: Logueado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 *              '400':
 *                  description: Email o contraseña incorrectos
 * 
 */
usuarioRouter.get('/agenda/:id', async (req, res) => {
    const agenda = await db.Usuario.findOne(
        {
            where:
                { id: req.params.id },
            include: [
                {
                    model: db.Direccion, as: "Direccion"
                }
            ]
        }
    );
    res.json(agenda);
});

/**
 * @swagger
 * paths:
 *  /usuarios/login:
 *      post:
 *          summary: Login de usuario antes registrado
 *          security: []
 *          requestBody: 
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              email: 
 *                                  type: string
 *                              pass: 
 *                                  type: string
 *                          example: 
 *                              email: mayra123@nada.com
 *                              pass: 'mayra123'
 *          tags: [Usuarios]
 *          responses:
 *              '201':
 *                  description: Logueado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 *              '400':
 *                  description: Email o contraseña incorrectos
 * 
 */

usuarioRouter.post('/login', async (req, res) => {
    try {

        const { email, pass } = await loginSchema.validateAsync(req.body);
        const { id, nombreUsuario, pass: passw, isAdmin, activo } = await db.Usuario.findOne({ where: { email } });
        const resultado = bcrypt.compareSync(pass, passw);
        // console.log(resultado, activo);
        if (!resultado) { res.status(401).json("contraseña Invalida") }
        if (!activo) { res.status(401).json("usuario NO activo") }
        const token = jwt.sign({
            id,
            nombreUsuario,
            isAdmin,
        }, config.module.JWTSECRET);
        res.json({ token });
    } catch (error) {
        res.status(404).json(error);

    }

});

/**
 * @swagger
 * paths:
 *  /usuarios/suspender:
 *      put:
 *          summary: Suspender o inhabilitar usuario
 *          requestBody: 
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id: 
 *                                  type: integer
 *                              activo:
 *                                  type: boolean
 *                          example: 
 *                              id: 1
 *                              activo: false    
 *          tags: [Administradores]
 *          responses:
 *              '201':
 *                  description: Usuario suspendido con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */


usuarioRouter.put('/suspender', verifyAdmin, async (req, res) => {
    try {
        const usuarioSuspendido = await db.Usuario.update(
            { activo: req.body.activo }, {
            where: {
                id: req.body.id
            }
        }
        );
        res.json({ "Estado de usuario modificado": usuarioSuspendido });
    }
    catch (error) {
        res.status(400).json(error.details[0])
    }
});

module.exports = usuarioRouter;