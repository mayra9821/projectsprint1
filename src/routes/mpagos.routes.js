const express = require( 'express');
const verifyAdmin = require( '../middlewares/auth');
const db = require( '../utils/db');


const mPagosRouter = express.Router();

/**
 * @swagger
 * paths:
 *  /mpagos:
 *      get:
 *          summary: obtener medios de pago
 *          tags: [Medios de pago]
 *          responses:
 *              '201':
 *                  description: JSON con medios de pago
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */
mPagosRouter.get('/', async (req,res) => {
    if (req.user.isAdmin) {
        const payM = await db.MetodoPago.findAll();

    }else{
        const payM = await db.MetodoPago.findAll({where: {activo: true}},{activo: 0});
    }
    res.json(payM);
});
/**
 * @swagger
 * paths:
 *  /mpagos:
 *      post:
 *          summary: cargar nuevo medio de pago
 *          tags: [Administradores]
 *          responses:
 *              '201':
 *                  description: medio de pago cargado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */
mPagosRouter.post('/', verifyAdmin, async (req,res) => {
    try {
        const payMethodCreado = await db.MetodoPago.create(
            req.body
        );
        res.json({"metodo creado": payMethodCreado});
    }catch (err) {
        res.status(400).json(err);
    }
});
/**
 * @swagger
 * paths:
 *  /mpagos/update:
 *      put:
 *          summary: modificar medio de pago por id
 *          requestBody: 
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id: 
 *                                  type: integer
 *                              activo:
 *                                  type: boolean
 *          tags: [Administradores]
 *          responses:
 *              '201':
 *                  description: pedido modificado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */

mPagosRouter.put('/update', verifyAdmin, async (req,res) => {
    try {
        const payActualizado = await db.MetodoPago.update(
            req.body, {
                where: {
                    id: req.body.id
                }
            }
        );
        res.json("metodo de pago actualizado");
    } catch (error) {
        res.status(400).json(error);
    }

});



module.exports = mPagosRouter;