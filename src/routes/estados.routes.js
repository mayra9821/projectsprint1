const express = require( 'express');
const verifyAdmin = require( '../middlewares/auth');
const db = require( '../utils/db');


const estadosRouter = express.Router();

/**
 * @swagger
 * paths:
 *  /estados:
 *      get:
 *          summary: obtener estados de pedidos con credenciales de administrador
 *          tags: [Administradores]
 *          responses:
 *              '201':
 *                  description: JSON con estados de pedido
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */
estadosRouter.get('/', verifyAdmin, async (req,res) => {
    const estado = await db.Estado.findAll();
    res.json(estado);
});
/**
 * @swagger
 * paths:
 *  /estados:
 *      post:
 *          summary: cargar nuevo estado de pedido
 *          tags: [Administradores]
 *          responses:
 *              '201':
 *                  description: pedido modificado con exito
 *                  content: 
 *                      application/json:
 *                          schema: 
 *                              type: object
 *                              properties:
 *                                  msg:
 *                                      type: string
 */
estadosRouter.post('/', verifyAdmin, async (req,res) => {
    try {
        const estadosCreado = await db.Estado.create(
            req.body
        );
        res.json({"metodo creado": estadosCreado});
    }catch (err) {
        res.status(400).json(err);
    }
});





module.exports = estadosRouter;
