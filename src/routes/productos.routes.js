const express = require( 'express');
const redis = require( 'redis');
const verifyAdmin = require( '../middlewares/auth');
const db = require( '../utils/db');
const config = require( '../config/config');
const client = redis.createClient(config.module.REDISPORT);


//log error to the console if any occurs
client.on("error", (err) => {
    console.log(err);
});

console.log('ping a redis:', client.ping())

const productsRouter = express.Router();

const actualizarCache = (key, data) => {
    try{
        client.set(key,data)  
    } catch (err) {
        console.log(err);
    }
}

/**
 * @swagger
 * /productos:
 *  get:
 *      summary: obtener la lista de productos con credenciales de usuario
 *      tags: [Productos]
 *      responses:
 *          '200':
 *              description: JSON con lista de productos disponibles
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 */
productsRouter.get('/', async (req,res) => {
    client.exists('products', async (err, exists) =>{
        if(exists){
            console.log("cache hit");
            client.get('products', async (err, data) => {
                res.json(JSON.parse(data));
            });
        }else{
            const products = await db.Producto.findAll();
            res.json(products);
            client.set('products', JSON.stringify(products));
            console.log("cache miss");
        }
    })
});

/**
 *@swagger
 * /productos:
 *  post:
 *      summary: Crear nuevo producto con credenciales de administrador
 *      tags: [Administradores]
 *      requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              nombre:
 *                                  type: string
 *                              precio: 
 *                                  type: integer
 *                          example:
 *                              nombre: 'pixirra'
 *                              precio: 5000
 *      responses:
 *          '200':
 *              description: JSON con producto creado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          
 *          '400':
 *              description: Error, debe ser administrador para crear un producto
 *                  
 */
productsRouter.post('/', verifyAdmin, async (req,res) => {
    try{
        const nuevoProducto = await db.Producto.create(req.body);
        res.json({'Producto creado':nuevoProducto});
        actualizarCache('products', JSON.stringify(await db.Producto.findAll()));
    }catch(err){
        res.status(400).send(err);
    }
});

/**
 *@swagger
 * /productos/update:
 *  put:
 *      summary: actualizar producto con credenciales de administrador
 *      tags: [Administradores]
 *      requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: integer
 *                              precio: 
 *                                  type: integer
 *                              activo:
 *                                  type: boolean
 *                          example:
 *                              id: 1
 *                              precio: 18000
 *                              activo: false
 *      responses:
 *          '200':
 *              description: JSON con producto actualizado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          
 *          '400':
 *              description: Error, debe ser administrador para crear un producto
 *                  
 */

productsRouter.put('/update', verifyAdmin, async (req,res) => {
    try{
        const productoActualizado = await db.Producto.update(
            req.body, {
                where: {
                    id : req.body.id
                }
            });
        res.json({'Producto actualizado':productoActualizado});
        actualizarCache('products', JSON.stringify(await db.Producto.findAll()));
    }catch(err){
        res.status(400).send(err);
    }
});

/**
 *@swagger
 * /productos/delete:
 *  delete:
 *      summary: borrar producto con credenciales de administrador
 *      tags: [Administradores]
 *      requestBody:
 *              required: true
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: object
 *                          properties:
 *                              id:
 *                                  type: integer
 *                          example:
 *                              id: 1
 *      responses:
 *          '200':
 *              description: JSON con producto borrado
 *              content:
 *                  application/json:
 *                      schema:
 *                          type: array
 *                          
 *          '400':
 *              description: Error, debe ser administrador para borrar un producto
 *                  
 */

productsRouter.delete('/delete', verifyAdmin, async (req, res) => {
    try{
        const productoBorrado = await db.Producto.destroy({ 
            where:
            {
                id : req.body.id
            }
        }
    )
    res.json({"Producto eliminado":productoBorrado});
    actualizarCache('products', JSON.stringify(await db.Producto.findAll()));
    }catch(err){
        res.status(400).send(err);
    }
})


module.exports = productsRouter;
