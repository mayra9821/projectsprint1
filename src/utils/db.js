const config = require( '../config/config');
const Config=config.module;

const db = {};


const {Sequelize, DataTypes} = require('sequelize');
const sequelize = new Sequelize(Config.DB_database, Config.DB_user, Config.DB_password, {
	host: Config.DB_host , 
        dialect: 'mysql',
        logging:false
      });

db.sequelize = sequelize;
db.Sequelize = Sequelize;


db.Usuario = require('../models/usuario.model')(sequelize, DataTypes);
db.Producto = require('../models/producto.model')(sequelize,DataTypes);
db.MetodoPago = require('../models/mpago.model')(sequelize, DataTypes);
db.Pedido = require('../models/pedido.model')(sequelize, DataTypes);
db.Estado = require('../models/estado.model')(sequelize, DataTypes);
db.Direccion = require('../models/direccion.model')(sequelize, DataTypes);


// Tabla de Relacione (through)
db.Productoxpedido = require ('../models/productosxpedido.model')(sequelize, DataTypes);



db.Usuario.hasMany(db.Pedido);
db.Pedido.belongsTo(db.Usuario);

db.Usuario.hasMany(db.Direccion,{as:'Direccion'} );
db.Direccion.belongsTo(db.Usuario);

db.Direccion.hasMany(db.Pedido, {
  foreignKey: {name:'direccionId'}
});
db.Pedido.belongsTo(db.Direccion, { as:'Direccion',
  foreignKey: {name:'direccionId'}
});


db.Estado.hasMany(db.Pedido);
db.Pedido.belongsTo(db.Estado);

db.MetodoPago.hasMany(db.Pedido);
db.Pedido.belongsTo(db.MetodoPago);

db.Producto.belongsToMany(db.Pedido, { through: db.Productoxpedido})
db.Pedido.belongsToMany(db.Producto, { through: db.Productoxpedido})

module.exports = db;
