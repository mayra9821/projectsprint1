const swaaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: 'Delilah Resto V2.0',
            version: '2.0.0',
        },
        servers: [
            {
                url: "../",
                description: "Servidor local"
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: "http",
                    scheme: "bearer",
                    bearerFormat: "JWT"
                }
            }
        },
        security: [
            {
                bearerAuth: []
            }
        ],
        tags: [
            {
                name: "Usuarios",
                description: "Modulo de registro e inicio de sesion de usuarios"
            },
            {
                name: "Administradores",
                description: "Modulo exclusivo para administradores, debe ingresar con credenciales de administrador"
            },
            {
                name: "Medios de pago",
                description: "Modulo de medios de usuarios"
            },
            {
                name: "Pedidos",
                description: "Modulo de pedidos de usuarios"
            },
            {
                name: "Productos",
                description: "Modulo de productos para usuarios"
            }
        ]
    },
    apis: ['./src/routes/*.js'], 
};


module.exports = swaaggerOptions;
