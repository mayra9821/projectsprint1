module.exports = (sequelize,DataTypes) => {
    const MetodoPago = sequelize.define("metodopago",{
        metodo:{
            type: DataTypes.STRING
        },
        activo:{
            type: DataTypes.BOOLEAN,
            defaultValue: true

        }
    })
    return MetodoPago;
}