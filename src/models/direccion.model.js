module.exports = (sequelize,DataTypes) => {
    const Direccion = sequelize.define("direcciones",{
        direccion:{
            type: DataTypes.STRING
        }
    })
    return Direccion;
}