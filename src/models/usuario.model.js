module.exports = (sequelize,DataTypes) => {
    const Usuario = sequelize.define("usuarios",{
        nombreUsuario:{
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        email:{
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
            validate:{
                isEmail: true
            }
        },
        pass:{
            type: DataTypes.STRING,
            allowNull: false,
        },
        telefono:{
            type: DataTypes.INTEGER,
            validate:{
                isNumeric: true,
            }
        },
        isAdmin:{
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        },
        activo:{
            type: DataTypes.BOOLEAN,
            defaultValue: true
        }
    })
    return Usuario;
}



