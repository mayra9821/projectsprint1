module.exports = (sequelize,DataTypes) => {
    const Producto = sequelize.define("productos",{
        nombre:{
            type: DataTypes.STRING
        },
        precio:{
            type: DataTypes.INTEGER
        },
        activo:{
            type: DataTypes.BOOLEAN,
            defaultValue: true
        }
    })
    return Producto;
}
