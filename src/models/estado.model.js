module.exports = (sequelize,DataTypes) => {
    const Estado = sequelize.define("estados",{
        nombre:{
            type: DataTypes.STRING
        }     
    }, {
        timestamps: false
    });
    return Estado;
}
