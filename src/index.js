const express = require( 'express');
const helmet = require( 'helmet');
const mysql2 = require( 'mysql2');
const expressJwt = require( 'express-jwt');
const db = require( './utils/db');
const usuarioRouter = require( './routes/usuarios.routes');
const paymentsRouter = require( './routes/mpagos.routes');
const productsRouter = require( './routes/productos.routes');
const pedidosRouter = require( './routes/pedidos.routes');
const estadosRouter = require( './routes/estados.routes');
const config = require( './config/config');
const swaggerJsDoc = require( 'swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const swaggerOptions = require( './utils/swaggerOptions');

const swaggerSpecs = swaggerJsDoc(swaggerOptions);


const PORT = parseInt(config.module.PORT) || 3000;

const misuperpass = config.module.JWTSECRET;

const app = express();

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));
app.use(helmet());
app.use(express.json());
app.use(
    expressJwt({
        secret: misuperpass,
        algorithms: ['HS256'],
    }).unless({
        path: ['/usuarios/login', '/usuarios/registro','/'],
    })
);
app.use('/usuarios', usuarioRouter);
app.use('/mpagos', paymentsRouter);
app.use('/productos', productsRouter);
app.use('/pedidos', pedidosRouter);
app.use('/estados', estadosRouter);
app.get('/',(req,res) => {
    res.send('<h1>Ingresa a la documentación de la api: <br/> <a href="/api-docs" >click aqui</a></h1>')
})


const createDb = async () => {
    const connection = mysql2.createConnection({
        host: config.module.DB_host,
        user: config.module.DB_user,
        password: config.module.DB_password
    });
    connection.connect();
    await connection.promise().query(`CREATE database IF NOT exists ${config.module.DB_database};`);
    connection.end();
}
createDb().then(() => {
    console.log("Base de datos Creada o Encontrada")
    db.sequelize.sync({
            force: false
        })
        .then(() => {
            console.log('Conectado a la base de datos');
            app.listen(PORT);
            (async function defaults() {
                try {
                    require('./defaults/usuarios.default')();
                    require('./defaults/productos.default')();
                    require('./defaults/estados.default')();
                    require('./defaults/mpagos.default')();
                    console.log(`Escuchando en el puerto ${PORT}`);
                } catch (error) {
                    console.error('Unable to connect to the database:', error);
                }
            })()
        })
        .catch(err => {
            console.log('error conectando a la BD: ' + err);
        });
}).catch(err => {
    console.log('error conectando a la BD: ' + err);
})

module.exports = app;