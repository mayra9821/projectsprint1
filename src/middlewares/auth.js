function verifyAdmin(req, res, next) {
    if (req.user.isAdmin) {
        next();
    }else{
        res.sendStatus(403);
    }
}

module.exports = verifyAdmin;