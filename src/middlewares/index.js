const express = require( 'express');
const helmet = require( 'helmet');
const expressJwt = require( 'express-jwt');
const db = require( './utils/db');
const usuarioRouter = require( './routes/usuarios.routes');
const paymentsRouter = require( './routes/mpagos.routes');
const productsRouter = require( './routes/productos.routes');
const pedidosRouter = require( './routes/pedidos.routes');
const estadosRouter = require( './routes/estados.routes');
const config = require( './config/config');

//instalar helmet, poner capa 

const PORT = parseInt(config.module.PORT) || 3000;

const misuperpass = config.module.JWTSECRET; 

const app = express();
app.use(helmet());

app.use(express.json());
app.use(
    expressJwt({
    secret: misuperpass,
    algorithms: ['HS256'],
    }).unless({
        path: ['/usuarios/login', '/usuarios/registro/'],
    })   
);
app.use('/usuarios', usuarioRouter);
app.use('/mpagos', paymentsRouter);
app.use('/productos', productsRouter);
app.use('/pedidos',pedidosRouter);
app.use('/estados',estadosRouter);


// const swaggerJsDoc = require('swagger-jsdoc');
//const swaggerUi = require('swagger-ui-express');
//const swaggerOptions = require('./utils/swaggerOptions');


db.sequelize.sync({ force: false })
    .then(() => {
        console.log('Conectado a la base de datos');
        app.listen(PORT);
        (async function defaults(){
        try {
        require('./defaults/usuarios.default')();
        require('./defaults/productos.default')();
        require('./defaults/estados.default')();
        require('./defaults/mpagos.default')();
        console.log(`Escuchando en el puerto ${PORT}`);
        }catch (error) {
            console.error('Unable to connect to the database:', error);
          }
        })()
        
        
    })
    .catch(err => {
        console.log('error conectando a la BD: '+ err);
    });

//const swaggerSpecs = swaggerJsDoc(swaggerOptions);

//app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpecs));