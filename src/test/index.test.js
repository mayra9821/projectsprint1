
const chai = require( 'chai');
const chaiHttp = require( 'chai-http');
const app = require ('../index');
const db = require('../utils/db');

chai.should();
chai.use(chaiHttp);

describe('test para registro de usuarios', () => {
    describe('POST registro de forma exitosa', () => {
        it('Esta función deberia devolver un 200 en status', (done) => {
            const usuarioCreadoTest = {
                "nombreUsuario" : "test",
                "email": "test@nada.com",
                "pass": "test",
                "direccion": "probando 12",
                "telefono":"12312312",
            };
            chai.request(app)
                .post('/usuarios/registro')
                .send(usuarioCreadoTest)
                .end((err,response) => {
                    response.should.have.status(200);
                    response.should.be.an('object');
                    done();
                });
        });
    }); 
    describe('POST registro de forma erronea', () => {
        it('Esta función deberia devolver un 400 en status', (done) => {
            const usuarioCreadoTest2 = {
                "nombreUsuario" : "R",
                "email": "dfgksdg@tres.com",
                "pass": "er",
                "direccion": "por al aca",
                "telefono":"",
            };
                chai.request(app)
                .post('/usuarios/registro')
                .send(usuarioCreadoTest2)
                .end((err,response) => {
                    // (err === null).should.be.true;
                    response.should.have.status(400);
                    done();
                });
        });
    });
    after (async () =>{
        await db.Usuario.destroy({ 
            where: { 
                email: 'test@nada.com' 
            }
        });
    });
});
