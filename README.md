# Api de Prueba en Javascript

_En el repositorio encontraremos una api desarrollada en Express que permite la creacion de pedidos (usuarios), y la administracion de usuarios, pedidos, metodos de pago y productos_

## Dominio

_El dominio creado para este proyecto es: "delilahresto.tk"_

## URL repositorio git
_https://gitlab.com/mayra9821/projectsprint1_


## Clonación codigo fuente
```
git clone https://gitlab.com/mayra9821/projectsprint1.git
```

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento  para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Para desplegar el proyecto debe conectarse por ssh a la ip publica de la instancia EC2, esto lo encontrará en EC2-->instancias-->direccion IPv4 publica_

_Adicional a esto, para probar la capa de caché debe tener encendido el servidor de Redis en su computador_

_El archivo PEM lo encontrará en la carpeta que esta recibiendo como evidencia del proyecto_


### Ejecucion 🔧

_Como en la instacia ya esta el repositorio clonado solo es hacer las pruebas correspondientes de los story points_
_Es importante saber que el story point de Cloudfront, s3 y CI no se hicieron por deficiencias en el conocimiento necesario para ejecutarlo_

## Despliegue 📦

_Una vez esté en la instacia EC2 el codigo ya estará ejecutandose con PM2_

_Al hacerlo (si no se presenta ningun error) se mostrara en la salida de la terminal un mensaje como este_


```
> nodemon src/index.js --exec babel-node

[nodemon] 2.0.14
[nodemon] to restart at any time, enter `rs`
[nodemon] watching path(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `babel-node src/index.js`
Base de datos Creada o Encontrada
Conectado a la base de datos
Escuchando en el puerto 3000
```
_Esto indicara el puerto en el cual se esta ejecutando el proyecto, para acceder a el basta con utilizar cualquier navegador y entrar a la direccion_ [localhost:3000](https://localhost:3000)
## Construido con 🛠️

* [Node.js](https://nodejs.org/es/docs/) - Entorno de programación
* [Express](https://maven.apache.org/) - Framework de Javascript
* [Swagger](https://swagger.io/docs/) - Usado para documentar la API


## Autores ✒️

* **Mayra Torres** - *Desarrollo y documentación de la API* - [mayra9821](https://gitlab.com/mayra9821)


